# Auto Save Remote Images (Drafts)

WordPress plugin for downloading automatically first remote image from a post and setting it as a featured image (only when the post is saved as a draft).

http://www.fernan.com.es/wordpress/

Based on Auto Save Remote Images plugin by Prebhdev Singh.

https://wordpress.org/plugins/auto-save-remote-images/

No longer overwriting a post existing featured image function included.

https://github.com/cristoslc/wp-auto-save-remote-image

## Description

The purpose of this plugin is very simple - when a post is saved (not published), it will fetch the first remote/external image that is referenced.  The image that is retrieved is then attached to the post as the featured image.  There are no settings to configure.

Please do not use this plugin to violate copyrights.

Sample uses: You maintain multiple blogs; copy paste the post contents from one blog to the other and this plugin will automatically save the first external image to the second blog.  There is no need to re-upload images to multiple sites.

This plugin can also retrieve and save images from sites like Flickr, Facebook, Picasa, etc.

## Installation

Extract the zip file and just drop the contents in the wp-content/plugins/ directory of your WordPress installation and then activate the Plugin from Plugins page.

## Contributors

> Contributors: Fernan Díez

> Donate link: http://www.fernan.com.es/wordpress/

> Tags: auto download, remote image, featured, attachment, save, draft, hotlink, images

> Requires at least: 3.0

> Tested up to: 4.2.2

> Stable tag: 1.0
